const tabContainer = document.querySelector('.tabs');
        const contentContainer = document.querySelector('.tabs-content');

        function addTab(tabName, tabContent) {
            const tabId = tabName.toLowerCase().replace(/\s/g, '-');
            const tabTitle = document.createElement('li');
            tabTitle.classList.add('tabs-title');
            tabTitle.textContent = tabName;
            tabTitle.setAttribute('data-tab', tabId);
            tabContainer.appendChild(tabTitle);

            const tabContentDiv = document.createElement('div');
            tabContentDiv.classList.add('tabs-content');
            tabContentDiv.setAttribute('id', tabId + '-content');
            tabContentDiv.innerHTML = `<p>${tabContent}</p>`;
            contentContainer.appendChild(tabContentDiv);
            
            tabTitle.addEventListener('click', () => {
                const tabTitles = document.querySelectorAll('.tabs-title');
                const tabContents = document.querySelectorAll('.tabs-content');
                tabTitles.forEach((t) => t.classList.remove('active'));
                tabContents.forEach((content) => content.classList.remove('active'));
                tabTitle.classList.add('active');
                const content = document.getElementById(tabId + '-content');
                content.classList.add('active');
            });
        }
        addTab('Akali', 'Akali text goes here.');
        addTab('Anivia', 'Anivia text goes here.');
        addTab('Draven', 'Draven text goes here.');